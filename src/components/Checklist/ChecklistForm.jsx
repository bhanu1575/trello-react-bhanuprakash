import {
  Button,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
} from "@chakra-ui/react";
import React, { useRef, useState } from "react";
import { TbCheckbox } from "react-icons/tb";
import { createNewChecklist } from "../../Api";

export default function ChecklistForm({ cardId, updateChecklists }) {
  const [isInvalidTitle, setIsInvalidTitle] = useState(false);
  const inputTitleRef = useRef(null);

  const handleSubmit = (e, onClose) => {
    e.preventDefault();
    if (
      inputTitleRef.current.value == "" ||
      inputTitleRef.current.value.startsWith(" ")
    ) {
      setIsInvalidTitle(true);
    } else {
      createNewChecklist(inputTitleRef.current.value, cardId)
        .then((response) => {
          updateChecklists(response.data);
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
      onClose();
      inputTitleRef.current.value = "";
      setIsInvalidTitle(false);
    }
  };

  const handleClose = (onClose) => {
    setIsInvalidTitle(false);
    onClose();
  };

  return (
    <Popover
      closeOnBlur={true}
      initialFocusRef={inputTitleRef}
      placement={"bottom"}
      returnFocusOnClose={false}
    >
      {({ isOpen, onClose }) => (
        <>
          {/* FORM TRIGGER */}

          <PopoverTrigger>
            <Button>
              <TbCheckbox /> Add checklist
            </Button>
          </PopoverTrigger>

          {/* CREATE CHECKLIST FORM */}

          {isOpen && (
            <PopoverContent
              onBlur={() => {
                setIsInvalidTitle(false);
              }}
            >
              <PopoverHeader>Add checklist</PopoverHeader>
              <PopoverCloseButton onClick={() => handleClose(onClose)} />
              <PopoverBody>
                <form
                  onSubmit={(e) => {
                    handleSubmit(e, onClose);
                  }}
                >
                  <label className="text-base" htmlFor="board-title">
                    Title <span className="text-red-500">*</span>
                  </label>
                  {/* ERROR MESSAGE */}
                  <p className={`text-red-700 ${!isInvalidTitle && "hidden"}`}>
                    Invalid title
                  </p>
                  {/* INPUT FIELD */}
                  <input
                    className="px-2 w-full h-[35px] bg-transparent border focus:outline-blue-600"
                    ref={inputTitleRef}
                    type="text"
                    id="board-title"
                    autoComplete="off"
                  />
                  {/* FORM SUBMIT BUTTON */}
                  <Button
                    type="submit"
                    mt={4}
                    colorScheme="blue"
                    onClick={(e) => {
                      handleSubmit(e, onClose);
                    }}
                  >
                    Create
                  </Button>
                </form>
              </PopoverBody>
            </PopoverContent>
          )}
        </>
      )}
    </Popover>
  );
}
