import {
  Button,
  Flex,
  Heading,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Progress,
  Spacer,
  Text,
} from "@chakra-ui/react";
import React, { useContext, useEffect, useState } from "react";
import { TbCheckbox } from "react-icons/tb";
import CreateCheckitemForm from '../CheckItem/CreateCheckitemForm'
import CheckItemComponent from "../CheckItem/CheckItemComponent";
import { getCheckitemsInAChecklist, sendReqToDeleteChecklist } from "../../Api";
import CardContext from "../../context/CardContext";

export default function ChecklistComponent({checklistData,deleteChecklists }) {
  
  const [checkItems, setCheckitems] = useState([]);
  const [completedCheckItems,setCompletedCheckitems] = useState(null)
  const {setTotalItems,setCompletedItems}= useContext(CardContext)

  useEffect(() => {
    getCheckitemsInAChecklist(checklistData.id)
    .then((response) => {
      console.log(response.data);
      let completedItems = response.data.filter(checkItemData=>checkItemData.state=='complete').length
      setCompletedCheckitems(completedItems)
      setCheckitems(response.data);
    })
    .catch((err) => {
      let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
      toast.error(errMsg, { theme: "colored" });
    });

  }, []);

  const deleteCheckitems = (id,isCompleted)=>{
    const newCheckitems = []
    let deleteditem = undefined;

    checkItems.forEach(checkItemData => {
      if(checkItemData.id!=id){
        newCheckitems.push(checkItemData)
      } else {
        deleteditem = checkItemData
      }
    })

    if(isCompleted){
      setCompletedCheckitems(completedCheckItems-1)
    }
    setCheckitems(newCheckitems)
  }
  //ADDING NEWLY CREATED CHECKITEM TO THE EXISTING ARRAY OF CHECKITEMS
  const updateCheckitems =(newCheckItem)=>{
    const newCheckitems = [...checkItems,newCheckItem]
    setCheckitems(newCheckitems)
  }

  const handleDelete = ()=>{
    sendReqToDeleteChecklist(checklistData.id)
    .then((response)=>{
      deleteChecklists(checklistData.id)
      setTotalItems(prev=>prev-checkItems.length)
      setCompletedItems(prev=>prev-completedCheckItems)
    })
    .catch((err) => {
      let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
      toast.error(errMsg, { theme: "colored" });
    });
  }



  let percentage = Math.floor((completedCheckItems/checkItems.length)*100)

  return (
    <div className="px-2 mb-4">
      {/* CHECKLIST HEADER */}
      <div className="checklist-header mb-2 flex gap-1 items-center">
        <TbCheckbox />
        {/* CHECKLIST NAME */}
        <Heading fontSize={"medium"}>{checklistData.name}</Heading>
        <Spacer />
        {/* CHECKLIST OPTIONS */}
        <Popover
          closeOnBlur={true}
          placement={"bottom"}
          returnFocusOnClose={false}
        >
          {({ isOpen, onClose }) => (
            <>
            {/* CHECKLIST OPTION TRIGGER */}
              <PopoverTrigger>
                <Button>Delete</Button>
              </PopoverTrigger>

              {/* CHECKLIST OPTION POPUP */}
              {isOpen && (
                <PopoverContent>
                  <PopoverHeader>Delete {checklistData.name}?</PopoverHeader>
                  <PopoverCloseButton />
                  <PopoverBody>
                    <Text>
                      Deleting a checklist is permanent and there is no way to
                      get it back.
                    </Text>
                  </PopoverBody>
                  <PopoverFooter>
                    {/* DELETE CHECKLIST BUTTON */}
                    <Button
                      w={'full'}
                      type="submit"
                      colorScheme="red"
                      onClick={() => {
                        onClose();
                        handleDelete();
                      }}
                    >
                      Delete checklist
                    </Button>
                  </PopoverFooter>
                </PopoverContent>
              )}
            </>
          )}
        </Popover>
      </div>
      {/* CHECKLIST PROGRESS BAR */}
      <Flex alignItems={"center"}>
        <span className="mr-2">{percentage ? percentage : 0}%</span>
        <Progress className="rounded-full" size={"sm"} w={"full"} colorScheme={percentage===100 ? 'green':'blue' } value={percentage ? percentage : 0} />
      </Flex>
      {/* CHECKITEMS CONTAINER */}
      {checkItems.map((checkItemData) => {
        return (
          <CheckItemComponent
            key={checkItemData.id}
            checklistData={checklistData}
            checkItemData={checkItemData}
            deleteCheckitems={deleteCheckitems}
            setCompletedCheckitems={setCompletedCheckitems}
          />
        );
      })}
      {/* ADD NEW CHECKITEM FORM */}
      <CreateCheckitemForm checklistId={checklistData.id} updateCheckitems={updateCheckitems}/>
    </div>
  );
}
