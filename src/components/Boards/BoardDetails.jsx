import React, { useEffect, useState } from "react";
import BoardsList from "./BoardsList";
import ListContainer from "../List/ListContainer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useLocation, useParams } from "react-router-dom";
import { getAllBoardsData } from "../../Api";
import { Spinner } from "@chakra-ui/react";

export default function BoardDetails() {
  const { id } = useParams();
  const { state } = useLocation(); // GETTING ALL BOARDS DATA FROM PREVIOUS PAGE
  const [requiredBoard, setRequiredBoard] = useState(undefined); //BOARD TO BE SHOWN
  const [boards, setboards] = useState(state); //ALL BOARDS DATA
  const [isLoading, setIsloading] = useState(true);
  const [isInvalidBoard, setIsInvalidBoard] = useState(false);

  useEffect(() => {
    if (!state) {
      getAllBoardsData()
        .then((res) => {
          let activeBoards = res.data.filter((board) => !board.closed);
          let wantedBoard = activeBoards.find(
            (boardData) => boardData.id == id
          );
          if (!wantedBoard) {
            setIsInvalidBoard(true);
          } else {
            setRequiredBoard(wantedBoard);
            setboards(activeBoards);
            setIsloading(false);
          }
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
    }
  }, []);

  // WHEN USER CLICKS ON A BOARD IN BOARDS LIST
  const handleBoardClick = (boardId) => {
    setRequiredBoard(boards.find((boardData) => boardData.id == boardId));
  };

  const deleteBoard = (id) => {
    const newBoards = boards.filter((boardData) => boardData.id != id);
    setboards(newBoards);
    setRequiredBoard({});
  };

  //ADDING NEWLY CREATED BOARD TO THE EXISTING ARRAY OF BOARDS
  const updateBoards = (newBoardData) => {
    setboards([...boards, newBoardData]);
  };

  if (state && !requiredBoard) {
    let wantedBoard = boards.find((boardData) => boardData.id == id);
    setRequiredBoard(wantedBoard);
    setIsloading(false);
  }
  return (
    <IsValidBoard
      isInvalidBoard={isInvalidBoard}
      isLoading={isLoading}
      boards={boards}
      deleteBoard={deleteBoard}
      updateBoards={updateBoards}
      handleBoardClick={handleBoardClick}
      boardData={requiredBoard}
    />
  );
}

function IsValidBoard({
  isInvalidBoard,
  isLoading,
  boards,
  deleteBoard,
  updateBoards,
  handleBoardClick,
  boardData,
}) {
  if (isInvalidBoard) {
    return <h1>Invalid BoardId</h1>;
  }
  if (isLoading) {
    return (
      <div className="h-screen grid place-items-center">
        <Spinner />
      </div>
    );
  }
  return (
    <div className="flex">
      <ToastContainer />
      <BoardsList
        boards={boards}
        deleteBoard={deleteBoard}
        updateBoards={updateBoards}
        handleBoardClick={handleBoardClick}
      />
      <ListContainer boardData={boardData} />
    </div>
  );
}
