import {
  Popover,
  PopoverTrigger,
  Button,
  Tabs,
  TabList,
} from "@chakra-ui/react";
import React, { useRef } from "react";
import CreateBoardForm from "../CreateBoardForm";
import BoardItem from "./BoardItem";
import { FaPlus } from "react-icons/fa";

export default function BoardsList({
  boards,
  updateBoards,
  deleteBoard,
  handleBoardClick,
}) {
  const inputTitleRef = useRef(null);
  return (
    <div className="min-h-[95vh] min-w-[250px] boards-list" >
      {/* BOARDS LIST HEADER PART */}

      <div className="boards-list-header border-b flex items-center justify-between px-4 ">
        <p className="font-black text-lg">Your boards</p>
        <Popover
          offset={[70, 0]}
          closeOnBlur={true}
          returnFocusOnClose={false}
          initialFocusRef={inputTitleRef}
          placement={"right"}
        >
          {({ isOpen, onClose }) => (
            <>
              {/* ADD NEW BOARD BUTTON */}

              <PopoverTrigger>
                <Button _hover={{ bg: "none" }} color={"inherit"} bg={"none"}>
                  <FaPlus />
                </Button>
              </PopoverTrigger>

              {/* CREATE BOARD FORM */}

              {isOpen && (
                <CreateBoardForm
                  inputTitleRef={inputTitleRef}
                  updateBoards={updateBoards}
                  onClose={onClose}
                />
              )}
            </>
          )}
        </Popover>
      </div>

          {/* BOARDS LIST */}

      <Tabs variant="unstyled" orientation="vertical">
        <TabList w={"full"}>
          {boards?.map((board) => {
            return (
              <BoardItem
                key={board.id}
                board={board}
                Allboards={boards}
                deleteBoard={deleteBoard}
                handleBoardClick={handleBoardClick}
              />
            );
          })}
        </TabList>
      </Tabs>
    </div>
  );
}
