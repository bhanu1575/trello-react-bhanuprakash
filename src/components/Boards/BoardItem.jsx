import {
  Button,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Tab,
  Text,
} from "@chakra-ui/react";
import { FaEllipsisH } from "react-icons/fa";
import { Link, useParams } from "react-router-dom";
import { getBoardURL } from "../../Api";
import axios from "axios";
import { toast } from "react-toastify";
import {sendReqToDeleteBoard} from '../../Api'

export default function BoardItem({
  board,
  Allboards,
  deleteBoard,
  handleBoardClick,
}) {
  const { id } = useParams();

  //WHEN USER WANTS TO DELETE THE BOARD
  const handleDelete = () => {
      sendReqToDeleteBoard(board.id)
      .then((response) => {
        deleteBoard(board.id);
      })
      .catch((err) => {
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      });
  };

  return (
    <Tab className={`${board.id == id ? "bg-slate-400 " : ""}`}>
      <div
        className="board-item w-full"
        onClick={() => {
          handleBoardClick(board.id);
        }}
      >
        {/* BOARD NAME */}
        <Link
          state={Allboards}
          to={`/boards/${board.id}`}
          className=" flex justify-between"
        >
          {board.name}

          {/* BOARD OPTIONS */}

          <Popover
            closeOnBlur={true}
            placement={"bottom"}
            returnFocusOnClose={true}
          >
            {({ isOpen, onClose }) => (
              <>
                {/* BOARD OPTIONS TRIGGER */}

                <PopoverTrigger>
                  <p
                    className={`board-options ${
                      isOpen ? "inline-block" : "hidden"
                    }`}
                  >
                    <FaEllipsisH />
                  </p>
                </PopoverTrigger>

                {/* BOARD OPTIONS DIALOG BOX */}

                {isOpen && (
                  <PopoverContent maxW={"250px"}>
                    <PopoverHeader>Delete {board.name}?</PopoverHeader>
                    <PopoverCloseButton />
                    <PopoverBody>
                      <Text>
                        Deleting a board is permanent and there is no way to get
                        it back.
                      </Text>
                    </PopoverBody>
                    <PopoverFooter>
                      <Button
                        w={"full"}
                        type="submit"
                        colorScheme="red"
                        onClick={(e) => {
                          onClose();
                          handleDelete();
                        }}
                      >
                        Delete board
                      </Button>
                    </PopoverFooter>
                  </PopoverContent>
                )}
              </>
            )}
          </Popover>
        </Link>
      </div>
    </Tab>
  );
}
