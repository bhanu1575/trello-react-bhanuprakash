import React, { useEffect, useState } from "react";
import { Button, Spacer, Text } from "@chakra-ui/react";
import Cardoptions from "../Cards/Cardoptions";
import { TbCheckbox } from "react-icons/tb";
import CardContext from "../../context/CardContext";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  PortalManager,
} from "@chakra-ui/react";
import ChecklistForm from "../Checklist/ChecklistForm";
import ChecklistComponent from "../Checklist/ChecklistComponent";
import {  getChecklistsInACard } from "../../Api";

export default function Card({ cardData, deleteCards }) {
  const [totalItems, setTotalItems] = useState(cardData.badges.checkItems); //TOTAL CHECKITEMS IN A CARD
  const [completedItems, setCompletedItems] = useState(
    cardData.badges.checkItemsChecked
  ); //COMPLETED CHECKITEMS IN A CARD
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [checklists, setChecklists] = useState([]);

  //WHEN USER WANT TO DELETE THE LIST
  const deleteChecklists = (id) => {
    const newChecklists = checklists.filter(
      (checklistData) => checklistData.id != id
    );
    setChecklists(newChecklists);
  };
  //ADDING NEWLY CREATED LIST TO THE EXISTING ARRAY OF LIST
  const updateChecklists = (newChecklist) => {
    setChecklists([...checklists, newChecklist]);
  };

  useEffect(() => {
    getChecklistsInACard(cardData.id)
      .then((response) => {
        setChecklists(response.data);
      })
      .catch((err) => {
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      });
  }, []);

  return (
    <CardContext.Provider value={{ setTotalItems, setCompletedItems }}>
      {/* CARD COMPONENT */}

      <Button
        className="myCard flex"
        onClick={onOpen}
        _hover={{ border: "2px solid blue" }}
        px={"2"}
        minH={"40px"}
        alignItems={"center"}
        h={"fit-content"}
        flexWrap={"wrap"}
        as={"li"}
      >
        {/* CARD NAME */}

        <Text
          maxW={"150px"}
          bg={"transparent"}
          style={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            display: "inline-block",
            verticalAlign: "bottom",
          }}
        >
          {cardData.name}
        </Text>
        <Spacer />
        <Cardoptions id={cardData.id} deleteCards={deleteCards} />

        {/* CHECKITEMS IN A CARD */}

        <div className="min-w-full">
          {totalItems ? (
            <div
              className={` ${
                completedItems === totalItems ? "text-white bg-green-700" : ""
              } flex items-center gap-2 mb-1 px-2 p-1 max-w-fit rounded-md  text-sm font-extralight `}
            >
              <TbCheckbox />
              <p>{`${completedItems}/${totalItems}`}</p>
            </div>
          ) : null}
        </div>
      </Button>

      {/* CHECKLIST POPUP */}

      <PortalManager>
        <Modal isOpen={isOpen} onClose={onClose} size={"xl"}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>{cardData.name}</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <div className="flex gap-4">
                {/* CHECKLISTS CONTAINER */}

                <div className="min-w-[70%] min-h-[80vh]">
                  {checklists.map((checklistData) => {
                    return (
                      <ChecklistComponent
                        key={checklistData.id}
                        checklistData={checklistData}
                        deleteChecklists={deleteChecklists}
                      />
                    );
                  })}
                </div>

                {/* CHECKLIST FORM */}

                <div>
                  <p>Add to card</p>
                  <ChecklistForm
                    cardId={cardData.id}
                    updateChecklists={updateChecklists}
                  />
                </div>
              </div>
            </ModalBody>
          </ModalContent>
        </Modal>
      </PortalManager>
    </CardContext.Provider>
  );
}
