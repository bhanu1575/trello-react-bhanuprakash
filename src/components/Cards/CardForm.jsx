import React, { useEffect, useRef, useState } from "react";
import { Button, Input } from "@chakra-ui/react";
import { IoClose } from "react-icons/io5";
import {  createCard } from "../../Api";
import { toast } from "react-toastify";

export default function CardForm({ listId, updateCards }) {
  const [isInvalidTitile, setIsInvalidTitle] = useState(false);
  const [cardFormVisiblity, setCardFormVisibility] = useState(false);
  const cardTitleRef = useRef(null);

  function handlesubmit(e) {
    e.preventDefault();
    if (
      cardTitleRef.current.value == "" ||
      cardTitleRef.current.value.startsWith(" ")
    ) {
      setIsInvalidTitle(true);
    } else {
      const name = cardTitleRef.current.value;
      createCard(name,listId)
        .then((response) => {
          updateCards(response.data);
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
      cardTitleRef.current.value = "";
      setIsInvalidTitle(false);
    }
  }
  const handleCardFormVisibility = () => {
    setCardFormVisibility((prev) => !prev);
    setIsInvalidTitle(false);
  };

  useEffect(() => {
    if (cardFormVisiblity && cardTitleRef.current) {   //INPUT FIELD SHOULD GET FOCUS WHENEVER THE CARDFORM OPENS
      cardTitleRef.current.focus();
    }
  }, [cardFormVisiblity]);

  return (
    <div className="mt-2">

      {/* ADD A NEW CARD BUTTON */}

      <Button
        w={"full"}
        onClick={handleCardFormVisibility}
        display={cardFormVisiblity ? "none" : "block"}
        colorScheme="blue"
      >
        Add a card
      </Button>

    {/* CREATECARD FORM */}

      <form
        onSubmit={handlesubmit}
        className={`${!cardFormVisiblity ? "hidden" : ""} max-w-[272px]`}
        action=""
      >
        <label htmlFor="card-title"></label>

      {/* CARD FORM INPUT FIELD */}

        <Input
          bg={"white"}
          borderColor={isInvalidTitile ? "red" : "inherit"}
          mb={"2"}
          ref={cardTitleRef}
          name="card-title"
          id="card-title"
          placeholder="Enter a title for this card..."
        ></Input>

          {/* CARD FORM SUBMIT BUTTON */}

        <Button type="submit" colorScheme="blue">
          Add card
        </Button>

        {/* CARD FORM CLOSE BUTTON */}

        <Button
          _hover={{ bg: "transparent" }}
          onClick={handleCardFormVisibility}
          bg={"transparent"}
        >
          <IoClose fontSize={"x-large"} />{" "}
        </Button>
      </form>
    </div>
  );
}
