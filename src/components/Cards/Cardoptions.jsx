import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverBody,
  Button,
  Portal,
} from "@chakra-ui/react";
import React from "react";
import { FaEllipsisH } from "react-icons/fa";
import {  sendReqToDeleteCard } from "../../Api";
import { toast } from "react-toastify";

export default function CardOptions({ id, deleteCards }) {
  const handleClick = (e,id, deleteCards) => {
    e.stopPropagation()
    sendReqToDeleteCard(id)
      .then((response) => {
        deleteCards(id);
      })
      .catch((err) => {
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      });
  };

  return (
    <Popover offset={[0, 20]} placement="right">
      {({ isOpen, onClose }) => (
        <>
          {/* CARD OPTION TRIGGER */}

          <PopoverTrigger>
            <Button
              className="card-options"
              opacity={0}
              bg={"transparent"}
              height={"30px"}
              onClick={(e)=>{e.stopPropagation()}}
            >
              <FaEllipsisH />
            </Button>
          </PopoverTrigger>
          {isOpen && (
            <Portal>
              <PopoverContent w={"fit-content"}>
                <PopoverBody padding={"0px"}>
                  {/* DELETE CARD BUTTON */}

                  <Button
                    size={"sm"}
                    colorScheme="red"
                    onClick={(e) => handleClick(e,id, deleteCards)}
                  >
                    Archive this card
                  </Button>
                </PopoverBody>
              </PopoverContent>
            </Portal>
          )}
        </>
      )}
    </Popover>
  );
}
