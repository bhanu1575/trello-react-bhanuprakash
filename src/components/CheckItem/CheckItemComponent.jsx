import {
  Button,
  Checkbox,
  Flex,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Spacer,
} from "@chakra-ui/react";
import React, { useContext, useEffect, useRef, useState } from "react";
import { FaEllipsisH } from "react-icons/fa";
import { sendReqToDeleteCheckItem, setStateOfCheckItem } from "../../Api";

import CardContext from "../../context/CardContext";
import { toast } from "react-toastify";

export default function CheckItemComponent({
  checklistData,
  checkItemData,
  deleteCheckitems,
  setCompletedCheckitems,
}) {
  const initialRef = useRef(null);
  const [isCompleted, setIsCompleted] = useState(
    checkItemData.state == "complete" ? true : false
  );
  const { setTotalItems, setCompletedItems } = useContext(CardContext);

  const handleDelete = (onClose) => {
    onClose();
    sendReqToDeleteCheckItem(checkItemData.idChecklist, checkItemData.id)
      .then((res) => {
        deleteCheckitems(checkItemData.id, isCompleted);
        setTotalItems((prev) => prev - 1);
        if (isCompleted) {
          setCompletedItems((prev) => prev - 1);
        }
      })
      .catch((err) => {
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      });
  };

  function handleChange() {
    if (isCompleted) {
      setCompletedCheckitems((prev) => prev - 1);
      setCompletedItems((prev) => prev - 1);
    } else {
      setCompletedCheckitems((prev) => prev + 1);
      setCompletedItems((prev) => prev + 1);
    }
    setStateOfCheckItem(
      checklistData.idCard,
      checkItemData.id,
      isCompleted
    )
    .catch((err) => {
      let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
      toast.error(errMsg, { theme: "colored" });
    });
    setIsCompleted((prev) => !prev);

  }

  return (
    <Flex gap={"2"} alignItems={"center"} mb={"2"}>
      {/* CHECKITEM CHECKBOX */}

      <Checkbox defaultChecked={isCompleted} onChange={handleChange} />
      <div className="w-full checkitem flex justify-between items-center">
        {/* CHECKITEM NAME */}

        <p className={`${isCompleted ? "line-through" : null}`}>
          {checkItemData.name}
        </p>
        <Spacer />

        {/* CHECKITEM OPTIONS */}

        <Popover
          closeOnBlur={true}
          initialFocusRef={initialRef}
          placement={"bottom"}
        >
          {({ isOpen, onClose }) => (
            <>
              <PopoverTrigger>
                <Button className="checkitem-options" opacity={0} size={"sm"}>
                  <FaEllipsisH />
                </Button>
              </PopoverTrigger>
              {isOpen && (
                <PopoverContent w={"200px"} zIndex={"popover"}>
                  <PopoverHeader>Item Options</PopoverHeader>
                  <PopoverCloseButton />
                  <PopoverBody>
                    <Button
                      onClick={() => {
                        handleDelete(onClose);
                      }}
                      ref={initialRef}
                      w={"full"}
                      colorScheme="red"
                    >
                      Delete this item
                    </Button>
                  </PopoverBody>
                </PopoverContent>
              )}
            </>
          )}
        </Popover>
      </div>
    </Flex>
  );
}
