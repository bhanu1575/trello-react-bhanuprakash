import React, { useContext, useEffect, useRef, useState } from "react";
import { Button, Flex, Heading, Input, Spacer } from "@chakra-ui/react";
import { IoClose } from "react-icons/io5";
import {  createCheckItem } from "../../Api";
import { toast } from "react-toastify";
import CardContext from "../../context/CardContext";

export default function CreateCheckitemForm({ checklistId, updateCheckitems }) {
  const [isInvalidTitile, setIsInvalidTitle] = useState(false);
  const [formVisiblity, setFormVisibility] = useState(false);
  const itemTitleRef = useRef(null);
  const { setTotalItems } = useContext(CardContext);

  function handlesubmit(e) {
    e.preventDefault();

    if (
      itemTitleRef.current.value == "" ||
      itemTitleRef.current.value.startsWith(" ")
    ) {
      setIsInvalidTitle(true);
      itemTitleRef.current.blur();
    } else {
      createCheckItem(checklistId, itemTitleRef.current.value)
        .then((response) => {
          updateCheckitems(response.data);
          setTotalItems((prev) => prev + 1);
          toast.success("checkitem created successfully");
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
      itemTitleRef.current.value = "";
      setIsInvalidTitle(false);
    }
  }

  const handleCardFormVisibility = () => {
    setFormVisibility((prev) => !prev);
    setIsInvalidTitle(false);
  };

  useEffect(() => {
    if (formVisiblity && itemTitleRef.current) {
      itemTitleRef.current.focus();
    }
  }, [formVisiblity]);

  return (
    <div className="mt-2">
      {/* ADD AN ITEM BUTTON */}

      <Button
        maxH={"30px"}
        w={"fit-content"}
        onClick={handleCardFormVisibility}
        display={formVisiblity ? "none" : "block"}
      >
        Add an item
      </Button>

      {/* CREATE CHECKITEM FORM */}

      <form
        onSubmit={handlesubmit}
        className={!formVisiblity ? "hidden" : "block"}
        action=""
      >
        <label htmlFor="card-title"></label>

        {/* FORM INPUT FIELD */}

        <Input
          placeholder="Add an item"
          borderColor={isInvalidTitile ? "red" : "inherit"}
          mb={"2"}
          ref={itemTitleRef}
          name="card-title"
          id="card-title"
        ></Input>

        {/* FORM SUBMIT BUTTON */}

        <Button type="submit">Add an item</Button>

        {/* FORM CLOSE BUTTON */}

        <Button
          _hover={{ bg: "transparent" }}
          onClick={handleCardFormVisibility}
          bg={"transparent"}
        >
          <IoClose fontSize={"x-large"} />{" "}
        </Button>
      </form>
    </div>
  );
}
