import React, { useEffect, useRef, useState } from "react";
import Listoptions from "./Listoptions";
import { Button, Flex, Heading, Input, Spacer } from "@chakra-ui/react";
import Card from "../Cards/Card";
import {  getCardsInList } from "../../Api";
import CardForm from "../Cards/CardForm";

export default function listComponent({ listData, deleteLists }) {
  const [cards, setCards] = useState([]);

  //DELETING THE CARDS
  const deleteCards = (id) => {
    const newCards = cards.filter((cardData) => cardData.id !== id);
    setCards(newCards);
  };

  //ADDING NEWLY CREATED CARDS TO THE EXISITING ARRAY OF CARDS
  const updateCards = (newcardData) => {
    setCards([...cards, newcardData]);
  };

  useEffect(() => {
    getCardsInList(listData.id)
      .then((response) => {
        console.log(response.data);
        setCards(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="h-fit bg-slate-300  min-w-[280px] rounded-md p-3">
      {/* LIST HEADER PART */}
      <div className="list-header flex justify-between items-center mb-2">
        <Heading fontSize={"large"}>{listData.name}</Heading>
        <Listoptions id={listData.id} deleteLists={deleteLists} />
      </div>
      {/* CARDS CONTAINER */}
      {cards.length ? (
        <ul className="flex flex-col gap-2">
          {cards.map((cardData) => {
            return (
              <Card
                key={cardData.id}
                cardData={cardData}
                deleteCards={deleteCards}
              />
            );
          })}
        </ul>
      ) : (
        <h1>no cards</h1>
      )}
      <CardForm listId={listData.id} updateCards={updateCards} />
    </div>
  );
}
