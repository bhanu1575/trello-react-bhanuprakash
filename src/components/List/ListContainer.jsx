import React, { useEffect, useRef, useState } from "react";
import ListComponent from "./ListComponent";
import { getListInBoard } from "../../Api";
import CreateListForm from "./CreateListForm";
import { toast } from "react-toastify";
export default function ListContainer({ boardData }) {
  const [lists, setLists] = useState([]);
  //DELETING THE LIST
  const deleteLists = (id) => {
    const newLists = lists.filter((list) => list.id != id);
    setLists(newLists);
  };
  //ADDING THE NEWLY CREATED LIST TO THE EXISTING ARRAY OF LIST
  const updateLists = (newlistData) => {
    setLists([...lists, newlistData]);
  };
  useEffect(() => {
    if (Object.keys(boardData).length) {
      const mainElement = document.body;
      if (boardData.prefs.backgroundImage) {
        mainElement.style.backgroundImage = `url(${boardData.prefs.backgroundImage})`;
      } else {
        mainElement.style.backgroundImage = "none";
        mainElement.style.backgroundColor = boardData.prefs.backgroundColor;
      }

      getListInBoard(boardData.id)
      .then((respsonse) => {
        setLists(respsonse.data);
      })
      .catch((err)=>{
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      })
    }
  }, [boardData]);

  return (
        <div className="list-container">
          {Object.keys(boardData).length ? (
            // LIST CONTAINER
            <div className="flex gap-3 ">
              {lists.map((listData) => {
                return (
                  <ListComponent
                    key={listData.id}
                    listData={listData}
                    deleteLists={deleteLists}
                  />
                );
              })}
              <CreateListForm
                updateLists={updateLists}
                boardId={boardData.id}
              />
            </div>
          ) : (
            <div className="grid place-items-center h-screen">
              <h1 className="bg-white text-4xl py-4 px-2 rounded-md">
                This Board is not available
              </h1>
            </div>
          )}
        </div>
      
  );
}
