import { Button, Flex, Input } from "@chakra-ui/react";
import React, { useEffect, useRef, useState } from "react";
import { IoAdd, IoClose } from "react-icons/io5";
import {  createNewlist } from "../../Api";
import { toast } from "react-toastify";

export default function CreateListForm({ updateLists, boardId }) {
  const [isInvalidTitile, setIsInvalidTitle] = useState(false);
  const [listFormVisiblity, setListFormVisbility] = useState(false);
  const listTitleRef = useRef(null);

  function handlesubmit(e) {
    e.preventDefault();

    if (
      listTitleRef.current.value == "" ||
      listTitleRef.current.value.startsWith(" ")
    ) {
      setIsInvalidTitle(true);
      listTitleRef.current.blur();
    } else {
      createNewlist(listTitleRef.current.value, boardId)
        .then((response) => {
          updateLists(response.data);
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
      listTitleRef.current.value = "";
      setIsInvalidTitle(false);
    }
  }

  const handlelistFormVisibility = () => {
    setListFormVisbility((prev) => !prev);
    setIsInvalidTitle(false);
  };

  useEffect(() => {
    if (listFormVisiblity && listTitleRef.current) {
      listTitleRef.current.focus();
    }
  }, [listFormVisiblity]);
  return (
    <>
      {/* ADD ANOTHER LIST BUTTON */}
      <Flex
        as={Button}
        className="bg-slate-300"
        onClick={handlelistFormVisibility}
        gap={"3"}
        minW={"200px"}
        display={listFormVisiblity ? "none" : "flex"}
      >
        <IoAdd /> Add another list{" "}
      </Flex>
      {/* CREATE NEW LIST FORM */}
      <form
        onSubmit={handlesubmit}
        className={`${
          !listFormVisiblity ? "hidden" : "block"
        } p-2 h-fit rounded-md min-w-56 bg-slate-300`}
        action=""
      >
        <label htmlFor="card-title"></label>
        {/* INPUT FIELD */}
        <Input
          autoComplete="off"
          bg={"white"}
          borderColor={isInvalidTitile ? "red" : "inherit"}
          mb={"2"}
          ref={listTitleRef}
          name="card-title"
          id="card-title"
          colorScheme="whiteAlpha"
          placeholder="Enter list title...."
        ></Input>
        {/* SUBMIT BUTTON */}
        <Button type="submit" colorScheme="blue">
          Add list
        </Button>
        {/* FORM CLOSE BUTTON */}
        <Button
          _hover={{ bg: "transparent" }}
          onClick={handlelistFormVisibility}
          bg={"transparent"}
        >
          <IoClose fontSize={"x-large"} />
        </Button>
      </form>
    </>
  );
}
