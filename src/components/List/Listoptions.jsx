import {
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverHeader,
    PopoverBody,
    PopoverFooter,
    PopoverCloseButton,
    Button,
    Portal,
    Box,
  } from '@chakra-ui/react'
import React from 'react'
import { FaEllipsisH} from 'react-icons/fa'

import {  sendReqToDeleteList } from '../../Api'
import axios from 'axios'
import { toast } from 'react-toastify'

export default function Listoptions({id,deleteLists}) {

    const handleClick = (id,onClose)=>{
      sendReqToDeleteList(id)
        .then((response)=>{
        deleteLists(response.data.id)
      })
      .catch((err)=>{
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
      })
      onClose();
    }
    
    return (
        <Popover  offset={[21,13]} placement='right'>
          {({ isOpen, onClose }) => (
            <>
            {/* LIST OPTIONS TRIGGER */}
              <PopoverTrigger>
                <Button bg={'transparent'}  height={'30px'}  ><FaEllipsisH /></Button>
              </PopoverTrigger>

              {/* LIST OPTION POPUP */}
              {
                isOpen && <Portal >
                <PopoverContent w={'180px'}>
                  <PopoverHeader>List options</PopoverHeader>
                  <PopoverCloseButton />
                  <PopoverBody>
                    {/* DELETE LIST BUTTON */}
                    <Button
                        w={'100%'}
                      colorScheme='red'
                      onClick={()=>handleClick(id,onClose)}
                    >
                      Archive this list
                    </Button>
                  </PopoverBody>
                </PopoverContent>
              </Portal>

              }
            </>
          )}
        </Popover>
      )
}
