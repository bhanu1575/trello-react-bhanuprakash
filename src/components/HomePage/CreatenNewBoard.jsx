import { Popover, PopoverTrigger, Button } from "@chakra-ui/react";
import React, { useRef } from "react";
import CreateBoardForm from "../CreateBoardForm";
import "react-toastify/dist/ReactToastify.css";

export default function CreatenNewBoard({ updateBoards }) {
  const inputTitleRef = useRef(null);


  return (
    <>
      <Popover
        closeOnBlur={true}
        returnFocusOnClose={false}
        initialFocusRef={inputTitleRef}
        placement={"right"}
      >
        {({ isOpen, onClose }) => (
          <>

              {/* FORM OPENING/CLOSING TRIGGER */}

            <PopoverTrigger>
              <Button minW={"191px"} h={"96px"}>
                Create new board
              </Button>
            </PopoverTrigger>

            {/* CREATE BOARD FORM */}
            {isOpen && (
              <CreateBoardForm
              inputTitleRef={inputTitleRef}
              updateBoards={updateBoards}
              onClose={onClose}
              />
            )}
          </>
        )}
      </Popover>
    </>
  );
}
