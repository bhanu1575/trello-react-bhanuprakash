import React, { useEffect, useState } from "react";
import CreatenNewBoard from "./CreatenNewBoard";
import { Link } from "react-router-dom";
import Userinfo from "./Userinfo";
import {  getAllBoardsData} from "../../Api";
import { Spinner } from "@chakra-ui/react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Homepage() {
  const [boards, setBoards] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const updateBoards = (newBoardData) => {
    setBoards([...boards, newBoardData]);
  };

  document.body.style.backgroundImage = "none";
  document.body.style.backgroundColor = "white";

  useEffect(() => {
      getAllBoardsData()
      .then((res) => {
        let activeBoards = res.data.filter((board) => !board.closed);
        setBoards(activeBoards);
        setIsLoading(false);
      })
      .catch((err) => {
        let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
        toast.error(errMsg, { theme: "colored" });
        setIsError(true);
      });
  }, []);

  return (
    <div className="max-w-[825px] mx-auto">
      <ToastContainer />
      <Userinfo />
      <h1>Your boards</h1>
      {isError ? (
        <div className="grid place-items-center h-[200px] w-full">
          Something went wrong
        </div>
      ) : (
        <Displayboards isLoading={isLoading} boards={boards} updateBoards={updateBoards} />
      )}
    </div>
  );
}

function Displayboards({ isLoading, boards, updateBoards }) {
  return (
    <>
      {isLoading ? (
        <div className="grid place-items-center h-[200px] w-full">
          <Spinner />
        </div>
      ) : (
        <div className="flex flex-wrap gap-2 max-w-[811px] mx-auto">
          {boards.map((boardData) => {
            let s = {
              backgroundColor: boardData.prefs.backgroundColor
                ? boardData.prefs.backgroundColor
                : "none",
              backgroundImage: boardData.prefs.backgroundImage
                ? `url(${boardData.prefs.backgroundImage})`
                : "none",
              backgroundSize: "100% 100%",
            };
            return (
              <Link
                state={boards}
                to={`/boards/${boardData.id}`}
                style={s}
                id={boardData.id}
                key={boardData.id}
                className="home-page-board"
              >
                {boardData.name}
              </Link>
            );
          })}
          <CreatenNewBoard updateBoards={updateBoards} />
        </div>
      )}
    </>
  );
}
