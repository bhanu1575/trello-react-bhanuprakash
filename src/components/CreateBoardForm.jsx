import {
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  Button,
  Portal,
} from "@chakra-ui/react";
import React, { useState } from "react";
import {  createNewBoard } from "../Api";
import { toast } from "react-toastify";

export default function CreateBoardForm({
  inputTitleRef,
  updateBoards,
  onClose,
}) {
  const [isInvalidTitile, setIsInvalidTitle] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      inputTitleRef.current.value == "" ||
      inputTitleRef.current.value.startsWith(" ")
    ) {
      setIsInvalidTitle(true);
    } else {
      createNewBoard(inputTitleRef.current.value)
        .then((res) => {
          updateBoards(res.data);
        })
        .catch((err) => {
          let errMsg = `${err.message} : could not ${err.config.method} ${err.config.url}`;
          toast.error(errMsg, { theme: "colored" });
        });
      onClose();
      inputTitleRef.current.value = "";
      setIsInvalidTitle(false);
    }
  };
  return (
    <Portal>
      <PopoverContent>
        <PopoverHeader>Create board</PopoverHeader>
        <PopoverCloseButton />
        <PopoverBody>
          <form
            onSubmit={(e) => {
              handleSubmit(e);
            }}
          >
            <label className="text-base" htmlFor="board-title">
              Board title <span className="text-red-500">*</span>
            </label>
            <p className={`text-red-700 ${!isInvalidTitile && "hidden"}`}>
              Invalid title
            </p>
            <input
              className="px-2 w-full h-[35px] bg-transparent border border-gray-500 focus:outline-blue-600"
              ref={inputTitleRef}
              type="text"
              id="board-title"
              autoComplete="off"
            />
            <Button
              type="submit"
              mt={4}
              colorScheme="blue"
              onClick={(e) => {
                handleSubmit(e, onClose);
              }}
            >
              Create
            </Button>
          </form>
        </PopoverBody>
      </PopoverContent>
    </Portal>
  );
}
