import React from 'react'
import { Link } from 'react-router-dom'

export default function Header() {

  const theme = {
    backgroundColor: '#1D2125',
    color: '#B6C3CE',
  }

  return (
    <header className='min-h-[5%] flex items-center px-2 border-b border-[#B6C3CE]' style={theme}>
      <Link to={'/'}>
        <img className="w-[75px] h-[32px] block icon py-2" src="https://trello.com/assets/d947df93bc055849898e.gif" alt="logo"/>
      </Link>
    </header>
 
  )
}