import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import {
  RouterProvider,
  createRoutesFromElements,
  createBrowserRouter,
  Route,
} from "react-router-dom";
import Homepage from "./components/HomePage/Homepage.jsx";
import { ChakraProvider } from "@chakra-ui/react";
import BoardDetails from "./components/Boards/BoardDetails.jsx";
import Errorpage from "./components/Errorpage.jsx";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />} errorElement={<Errorpage/>}>
      <Route path="" element={<Homepage />} />
      <Route path="/boards/:id" element={<BoardDetails />} />
    </Route>
  )
);

ReactDOM.createRoot(document.getElementById("root")).render(
  <ChakraProvider>
    <RouterProvider router={router} />
  </ChakraProvider>
);
