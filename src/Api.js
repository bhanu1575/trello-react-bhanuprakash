import axios from "axios";

const apiKey = "fef427d3e1db5c7b16320b906c7b0537";
const apiToken =
  "ATTA58eaeb8cd3970f9b59879dc61138c44d77078b34efff2d0e27b8b5e8cf964b041F063845";

function getBoardURL(id) {
  const url =  `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)

}

function getAllBoardsData() {
  const idmember = `662f78917455c90545182535`;
  const url =  `https://api.trello.com/1/members/${idmember}/boards?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)
 
}
function sendReqToDeleteBoard(id){
  const url = `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`;
  return axios.delete(url)

}
function getChecklistsInACard(id) {
  const url =  `https://api.trello.com/1/cards/${id}/checklists?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)

}

function createCard(name, listId) {
  const url =  `https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${apiToken}`;
  return axios.post(url)

}
function getCardData(id) {
  const url = `https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)

}
function sendReqToDeleteCard(id) {
  const url = `https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${apiToken}`;
  return axios.delete(url)

}

function sendReqToDeleteCheckItem(idChecklist, checkItemId) {
  const url =  `https://api.trello.com/1/checklists/${idChecklist}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`;
  return axios.delete(url)

}

function sendReqToDeleteChecklist(id) {
  const url = `https://api.trello.com/1/checklists/${id}?key=${apiKey}&token=${apiToken}`;
  return axios.delete(url)

}

function getCheckitemsInAChecklist(id) {
  const url =  `https://api.trello.com/1/checklists/${id}/checkItems?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)

}

function createNewChecklist(name, cardId) {
  const url =  `https://api.trello.com/1/checklists?name=${name}&idCard=${cardId}&key=${apiKey}&token=${apiToken}`;
  return axios.post(url)

}

function createNewlist(name,boardId){
    const url =  `https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`
    return axios.post(url)

}
function getCardsInList(id){
    const url =  `https://api.trello.com/1/lists/${id}/cards?key=${apiKey}&token=${apiToken}`
    return axios.get(url)

}
function sendReqToDeleteList(id){
    const url = `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${apiToken}&value=true`;
    return axios.put(url)


}
function createNewBoard(name){
    const url =  `https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${apiToken}`
    return  axios.post(url)

}

function setStateOfCheckItem(cardId, checkItemId, isCompleted) {
  const state = isCompleted ? "incomplete" : "complete";
  const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${apiKey}&token=${apiToken}`;
  return axios.put(url)
}

function createCheckItem(checklistId,name){
  const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${name}&key=${apiKey}&token=${apiToken}`;
  return axios.post(url)

}

function getListInBoard(boardId){
  const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;
  return axios.get(url)

}

export {
  getBoardURL,
  getAllBoardsData,
  getChecklistsInACard,
  createCard,
  getCardData,
  getListInBoard,
  sendReqToDeleteCheckItem,
  getCheckitemsInAChecklist,
  sendReqToDeleteChecklist,
  createNewChecklist,
  createNewlist,
  getCardsInList,
  sendReqToDeleteList,
  createNewBoard,
  sendReqToDeleteBoard,
  sendReqToDeleteCard,
  setStateOfCheckItem,
  createCheckItem
};
