import Header from "./components/Header"
import {Outlet} from 'react-router-dom'
function App() {


  return (
    <div className="h-screen">
      <Header/>
      <Outlet/>
    </div>
  )
}

export default App
